#include <stdio.h>
#include "ufuzzy.h"

/* Globals. */
/* Inputs. */
data_set input;
linguistic_variable lv_first;
term lv_first_term_low;
term lv_first_term_high;

linguistic_variable lv_second;
term lv_second_term_low;
term lv_second_term_high;

/* Outputs. */
data_set output;
linguistic_variable lv_output;
term lv_output_term_low;
term lv_output_term_high;

/* Rules. */
rule rule_first;
rule rule_second;
rule_set rules;


static void init_fuzzy() {
    /* Create second input linguistic variable. */
    lv_second_term_high = create_term(-10, 5, 5, 20, NULL);
    lv_second_term_low = create_term(-20, -5, -5, 10, &lv_second_term_high);
    lv_second = create_lv(&lv_second_term_low, NULL);
    /* Create first input linguistic variable. */
    lv_first_term_high = create_term(-10, 5, 5, 20, NULL);
    lv_first_term_low = create_term(-20, -5, -5, 10, &lv_first_term_high);
    lv_first = create_lv(&lv_first_term_low, &lv_second);
    /* Create output linguistic variable. */
    lv_output_term_high = create_term(-10,5,5,20, NULL);
    lv_output_term_low = create_term(-20, -5, -5, 10, &lv_output_term_high);
    lv_output = create_lv(&lv_output_term_low, NULL);

    /* Create rules */

}


// /* initialization function */
// void init_fuzzy(){

//     /* allocate rule set with 2 rules */
//     rules = alloc_rule_set(&input, &output, 2);
//     /* set first rule */
//     /* set_rule function uses variadic
//      - first, declare "if" side of rule (", TERM_HIGH, TERM_HIGH, ")
//      - then, declare "then" side of rule ("TERM_HIGH, TERM_LOW)");
//      length of sides have to correspond with number of inputs/outputs */
//     set_rule(&rules, 0, TERM_HIGH, TERM_HIGH, TERM_HIGH, TERM_LOW);
//     set_rule(&rules, 1, TERM_LOW, TERM_LOW, TERM_LOW, TERM_HIGH);
// }

int main() {
    /* Initialization. */
    init_fuzzy();
    /* Set input values. */
    lv_first.sharp_value = 5;
    lv_second.sharp_value = 5;
    /* Calculate values. */
    fuzzy_calculate(input, output, rules);
    /* Display result. */
    printf("Result: %f\n", lv_output.sharp_value);
    return 0;
}
