#ifndef _UFUZZY_H
#define _UFUZZY_H

/**
 * Term definition.
 */
typedef struct {
    double degree_of_membership;    /* Calculated degree of membership. */
    double point1;                  /* Left side of membership function. */
    double point2;                  /* Right side of membership function. */
    double slope1;                  /* Left slope. */
    double slope2;                  /* Right slope. */
    term* next;                     /* Pointer to next term. */
} term;

/**
 * Linguistic variable definition.
 */
typedef struct {
    double sharp_value;             /* Sharp value. If input - set externally, if output - computed. */
    term* term;                     /* Linked list of terms. */
    linguistic_variable* next;      /* Pointer to next linguistic variable. */
} linguistic_variable;

/**
 * Data set. Defines input or output of system.
 */
typedef linguistic_variable* data_set;

/**
 * Rule element. Constructs rule.
 */
typedef struct {
    term* term;                     /* Term used in rule. */
    rule_element* next;             /* Pointer to next rule_element. */
} rule_element;

/**
 * Rule.
 */
typedef struct {
    rule_element* if_side;
    rule_element* then_side;
    rule* next;
} rule;

/**
 * Rule set. Defines relation between input and output.
 */
typedef rule* rule_set;

/**
 * Creates term with calculation of slope.
 * @param   a       First point of membership trapezoid - left side, base.
 * @param   b       Second point of membership trapezoid - left side, top.
 * @param   c       Third point of membership trapezoid - right side, top.
 * @param   d       Fourth point of membership trapezoid - right side, base.
 * @param   next    Next element in linked list.
 */
term create_term(double a, double b, double c, double d, term* next);

/**
 * Creates linguistic variable.
 * @param   term    First term from linked list of terms.
 * @param   next    Pointer to next element in linked list of linguistic variables.
 */
linguistic_variable create_lv(term* term, linguistic_variable* next);

/**
 * Fuzzification of data. Calculates degree of membership for input terms.
 * @param   input       Input data set.
 */
void fuzzification(data_set* input);

/**
 * Zeroes degree of membership for all terms within data set.
 * Should be used on output after new calculation has begun.
 * @param   data_set    Data set to be zeroed.
 */
void reset_data_set(data_set* data_set);

/**
 * Calculates degree of membership for output terms based on defined rule set.
 * @param   input       Input data set.
 * @param   output      Output data set.
 * @param   rules       Rules set.
 */
void rule_evaluation(data_set* input, data_set* output, rule_set* rules);

/**
 * Defuzzification of data. Calculates sharp value.
 * @param   output      Output data set.
 */
void defuzzification(data_set* output);

/**
 * Main function.
 */
void fuzzy_calculate(data_set* input, data_set* output, rule_set* rules);

#endif // _UFUZZY_H
