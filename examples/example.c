#include <stdio.h>
#include "ufuzzy.h"

/* Globals. */
/* Inputs. */
struct linguistic_variable lv_first;
struct term lv_first_term_low;
struct term lv_first_term_high;

struct linguistic_variable lv_second;
struct term lv_second_term_low;
struct term lv_second_term_high;

/* Outputs. */
struct linguistic_variable lv_output;
struct term lv_output_term_low;
struct term lv_output_term_high;

/* Rules. */
/* First rule. */
struct rule_element rule_first_if_first;
struct rule_element rule_first_if_second;
struct rule_element rule_first_then_first;
struct rule rule_first;
/* Second rule. */
struct rule_element rule_second_if_first;
struct rule_element rule_second_if_second;
struct rule_element rule_second_then_first;
struct rule rule_second;

static void init_fuzzy() {
    /* Create second input linguistic variable. */
    lv_second_term_high = create_term(0, 10, 10, 20, NULL);
    lv_second_term_low = create_term(-20, -10, -10, 0, &lv_second_term_high);
    lv_second = create_lv(&lv_second_term_low, NULL);
    /* Create first input linguistic variable. */
    lv_first_term_high = create_term(0, 10, 10, 20, NULL);
    lv_first_term_low = create_term(-20, -10, -10, 0, &lv_first_term_high);
    lv_first = create_lv(&lv_first_term_low, &lv_second);
    /* Create output linguistic variable. */
    lv_output_term_high = create_term(0, 10, 10, 20, NULL);
    lv_output_term_low = create_term(-20, -10, -10, 0, &lv_output_term_high);
    lv_output = create_lv(&lv_output_term_low, NULL);

    /* Create rules */
    rule_second_if_second = create_rule_element(&lv_second_term_high, NULL);
    rule_second_if_first = create_rule_element(&lv_first_term_high, &rule_second_if_second);
    rule_second_then_first = create_rule_element(&lv_output_term_low, NULL);
    rule_second = create_rule(&rule_second_if_first, &rule_second_then_first, NULL);

    rule_first_if_second = create_rule_element(&lv_second_term_low, NULL);
    rule_first_if_first = create_rule_element(&lv_first_term_low, &rule_first_if_second);
    rule_first_then_first = create_rule_element(&lv_output_term_high, NULL);
    rule_first = create_rule(&rule_first_if_first, &rule_first_then_first, &rule_second);
}

int main() {
    /* Initialization. */
    init_fuzzy();
    /* Set input values. */
    lv_first.sharp_value = 0;
    lv_second.sharp_value = 0;
    /* Calculate values. */
    fuzzy_calculate(&lv_first, &lv_output, &rule_first);
    /* Display result. */
    printf("Result: %f\n", lv_output.sharp_value);
    return 0;
}
