cmake_minimum_required(VERSION 2.8)

project(ufuzzy_example C)

file(GLOB SOURCES 
    "*.c"
    "../src/*.c"
)
include_directories("../src/")

add_executable(${PROJECT_NAME} ${SOURCES})
