#include "ufuzzy.h"
#include "stdlib.h"
#include "stdarg.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define UPPER_LIMIT 255.0

static double compute_slope(double a, double b) {
    return UPPER_LIMIT / (b-a);
}

term create_term(double a, double b, double c, double d, term* next) {
    term t;
    t.point1 = a;
    t.point2 = d;
    t.slope1 = compute_slope(a,b);
    t.slope2 = compute_slope(c,d);
    t.degree_of_membership = 0.0;
    t.next = next;
    return t;
}

linguistic_variable create_lv(term* term, linguistic_variable* next) {
    linguistic_variable lv;
    lv.sharp_value = 0.0;
    lv.term = term;
    lv.next = next;
    return lv;
}

static void compute_degree_of_membership(double value, term* str) {
    double degree;
    double delta1 = value - str->point1;
    double delta2 = str->point2 - value;
    if (delta1 <= 0 || delta2 <= 0)
        degree = 0;
    else {
        degree = MIN(str->slope1 * delta1, str->slope2 * delta2);
        degree = MIN(degree, UPPER_LIMIT);
    }
    str->degree_of_membership = degree;
}

static double compute_area_of_trapezoid(term* str) {
    double base = str->point2 - str->point1;
    double run1 = str->degree_of_membership / str->slope1;
    double run2 = str->degree_of_membership / str->slope2;
    double top = base - run1 - run2;
    return str->degree_of_membership * (base + top) / 2.0;
}

void fuzzification(data_set* input){
    linguistic_variable* lv_pos = input;
    term* term_pos;

    while (lv_pos != NULL) {
        term_pos = lv_pos->term;
        while (term_pos != NULL) {
            compute_degree_of_membership(lv_pos->sharp_value, term_pos);
            term_pos = term_pos->next;
        }
        lv_pos = lv_pos->next;
    }
}

void reset_data_set(data_set* data_set) {
    linguistic_variable* lv_pos = data_set;
    term* term_pos;

    while (lv_pos != NULL) {
        term_pos = lv_pos->term;
        while (term_pos != NULL) {
            compute_degree_of_membership(0, term_pos);
            term_pos = term_pos->next;
        }
        lv_pos->sharp_value = 0;
        lv_pos = lv_pos->next;
    }
}

void rule_evaluation(data_set* input, data_set* output, rule_set* rules) {
    rule* rule_pos = rules;
    rule_element* if_side_pos;
    rule_element* then_side_pos;
    double strength = 0;
    
    while (rule_pos != NULL) {
        strength = UPPER_LIMIT;

        if_side_pos = rule_pos->if_side;
        while (if_side_pos != NULL) {
            strength = MIN(strength, if_side_pos->term->degree_of_membership);
            if_side_pos = if_side_pos->next;
        }

        then_side_pos = rule_pos->then_side;
        while (then_side_pos != NULL) {
            then_side_pos->term->degree_of_membership = MAX(strength, then_side_pos->term->degree_of_membership);
            then_side_pos = then_side_pos->next;
        }
        
        rule_pos = rule_pos->next;
    }
}

void defuzzification(data_set* output) {
    double area, sum_of_areas, sum_of_products, centroid;
    linguistic_variable* lv_pos = output;
    term* term_pos;

    while (lv_pos != NULL) {
        sum_of_areas = 0;
        sum_of_products = 0;
        term_pos = lv_pos->term;
        while (term_pos != NULL) {
            area = compute_area_of_trapezoid(term_pos);
            centroid = (term_pos->point1 + term_pos->point2) / 2.0;
            sum_of_products += centroid * area;
            sum_of_areas += area;
            term_pos = term_pos->next;
        }
        lv_pos->sharp_value = sum_of_areas != 0 ? sum_of_products / sum_of_areas : 0;
        lv_pos = lv_pos->next;
    }
}

void fuzzy_calculate(data_set* input, data_set* output, rule_set* rules) {
    fuzzification(input);
    reset_data_set(output);
    rule_evaluation(input, output, rules);
    defuzzification(output);
}
