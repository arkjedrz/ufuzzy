#ifndef _UFUZZY_H
#define _UFUZZY_H

/**
 * Term definition.
 */
struct term {
    double degree_of_membership;        /* Calculated degree of membership. */
    double point1;                      /* Left side of membership function. */
    double point2;                      /* Right side of membership function. */
    double slope1;                      /* Left slope. */
    double slope2;                      /* Right slope. */
    struct term* next;                  /* Pointer to next term. */
};

/**
 * Linguistic variable definition.
 */
struct linguistic_variable {
    double sharp_value;                 /* Sharp value. If input - set externally, if output - computed. */
    struct term* term;                  /* Linked list of terms. */
    struct linguistic_variable* next;   /* Pointer to next linguistic variable. */
};

/**
 * Rule element.
 */
struct rule_element {
    struct term* term;                  /* Term used in rule. */
    struct rule_element* next;          /* Pointer to next rule_element. */
};

/**
 * Rule.
 */
struct rule {
    struct rule_element* if_side;
    struct rule_element* then_side;
    struct rule* next;
};

/**
 * Creates term with calculation of slope.
 * @param   a       First point of membership trapezoid - left side, base.
 * @param   b       Second point of membership trapezoid - left side, top.
 * @param   c       Third point of membership trapezoid - right side, top.
 * @param   d       Fourth point of membership trapezoid - right side, base.
 * @param   next    Next element in linked list.
 */
struct term create_term(double a, double b, double c, double d, struct term* next);

/**
 * Creates linguistic variable.
 * @param   term    First term from linked list of terms.
 * @param   next    Pointer to next element in linked list of linguistic variables.
 */
struct linguistic_variable create_lv(struct term* term, struct linguistic_variable* next);

struct rule_element create_rule_element(struct term* term, struct rule_element* next);

struct rule create_rule(struct rule_element* if_side_first_element, struct rule_element* then_side_first_element, struct rule* next);

/**
 * Fuzzification of data. Calculates degree of membership for input terms.
 * @param   input       Input data set.
 */
void fuzzification(struct linguistic_variable* input);

/**
 * Zeroes degree of membership for all terms within list.
 * Should be used on output after new calculation has begun.
 * @param   list    Data set to be zeroed.
 */
void reset_terms(struct linguistic_variable* list);

/**
 * Calculates degree of membership for output terms based on defined rule set.
 * @param   input       Input data set.
 * @param   output      Output data set.
 * @param   rules       Rules set.
 */
void rule_evaluation(struct linguistic_variable* input, struct linguistic_variable* output, struct rule* rules);

/**
 * Defuzzification of data. Calculates sharp value.
 * @param   output      Output data set.
 */
void defuzzification(struct linguistic_variable* output);

/**
 * Main function.
 */
void fuzzy_calculate(struct linguistic_variable* input, struct linguistic_variable* output, struct rule* rules);

#endif // _UFUZZY_H
