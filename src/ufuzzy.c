#include "ufuzzy.h"
#include "stdlib.h"
#include "stdarg.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define UPPER_LIMIT 255.0

static double compute_slope(double a, double b) {
    return UPPER_LIMIT / (b-a);
}

struct term create_term(double a, double b, double c, double d, struct term* next) {
    struct term t;
    t.point1 = a;
    t.point2 = d;
    t.slope1 = compute_slope(a,b);
    t.slope2 = compute_slope(c,d);
    t.degree_of_membership = 0.0;
    t.next = next;
    return t;
}

struct linguistic_variable create_lv(struct term* term, struct linguistic_variable* next) {
    struct linguistic_variable lv;
    lv.sharp_value = 0.0;
    lv.term = term;
    lv.next = next;
    return lv;
}

struct rule_element create_rule_element(struct term* term, struct rule_element* next) {
    struct rule_element re;
    re.term = term;
    re.next = next;
    return re;
}

struct rule create_rule(struct rule_element* if_side_first_element, struct rule_element* then_side_first_element, struct rule* next) {
    struct rule r;
    r.if_side = if_side_first_element;
    r.then_side = then_side_first_element;
    r.next = next;
    return r;
}

static void compute_degree_of_membership(double value, struct term* str) {
    double degree;
    double delta1 = value - str->point1;
    double delta2 = str->point2 - value;
    if (delta1 <= 0 || delta2 <= 0)
        degree = 0;
    else {
        degree = MIN(str->slope1 * delta1, str->slope2 * delta2);
        degree = MIN(degree, UPPER_LIMIT);
    }
    str->degree_of_membership = degree;
}

static double compute_area_of_trapezoid(struct term* str) {
    double base = str->point2 - str->point1;
    double run1 = str->degree_of_membership / str->slope1;
    double run2 = str->degree_of_membership / str->slope2;
    double top = base - run1 - run2;
    return str->degree_of_membership * (base + top) / 2.0;
}

void fuzzification(struct linguistic_variable* input) {
    struct linguistic_variable* lv_pos = input;
    struct term* term_pos;

    while (lv_pos != NULL) {
        term_pos = lv_pos->term;
        while (term_pos != NULL) {
            compute_degree_of_membership(lv_pos->sharp_value, term_pos);
            term_pos = term_pos->next;
        }
        lv_pos = lv_pos->next;
    }
}

void reset_terms(struct linguistic_variable* list) {
    struct linguistic_variable* lv_pos = list;
    struct term* term_pos;

    while (lv_pos != NULL) {
        term_pos = lv_pos->term;
        while (term_pos != NULL) {
            compute_degree_of_membership(0, term_pos);
            term_pos = term_pos->next;
        }
        lv_pos->sharp_value = 0;
        lv_pos = lv_pos->next;
    }
}

void rule_evaluation(struct linguistic_variable* input, struct linguistic_variable* output, struct rule* rules) {
    struct rule* rule_pos = rules;
    struct rule_element* if_side_pos;
    struct rule_element* then_side_pos;
    double strength = 0;
    
    while (rule_pos != NULL) {
        strength = UPPER_LIMIT;

        if_side_pos = rule_pos->if_side;
        while (if_side_pos != NULL) {
            strength = MIN(strength, if_side_pos->term->degree_of_membership);
            if_side_pos = if_side_pos->next;
        }

        then_side_pos = rule_pos->then_side;
        while (then_side_pos != NULL) {
            then_side_pos->term->degree_of_membership = MAX(strength, then_side_pos->term->degree_of_membership);
            then_side_pos = then_side_pos->next;
        }
        
        rule_pos = rule_pos->next;
    }
}

void defuzzification(struct linguistic_variable* output) {
    double area, sum_of_areas, sum_of_products, centroid;
    struct linguistic_variable* lv_pos = output;
    struct term* term_pos;

    while (lv_pos != NULL) {
        sum_of_areas = 0;
        sum_of_products = 0;
        term_pos = lv_pos->term;
        while (term_pos != NULL) {
            area = compute_area_of_trapezoid(term_pos);
            centroid = (term_pos->point1 + term_pos->point2) / 2.0;
            sum_of_products += centroid * area;
            sum_of_areas += area;
            term_pos = term_pos->next;
        }
        lv_pos->sharp_value = sum_of_areas != 0 ? sum_of_products / sum_of_areas : 0;
        lv_pos = lv_pos->next;
    }
}

void fuzzy_calculate(struct linguistic_variable* input, struct linguistic_variable* output, struct rule* rules) {
    fuzzification(input);
    reset_terms(output);
    rule_evaluation(input, output, rules);
    defuzzification(output);
}
